# Deployment

The **A-Team pipeline** uses **OpenShift Pipelines** ([Tekton]) for building
and testing the different images.

The pipeline is defined as a set of tasks linked together in a specific
order. Some tasks will run in sequence, some in parallel.

For the integration with [Gitlab], the project uses a [webhook] which will
trigger the pipeline each time a developer files an MR (Merge Request), in
addition it can also be triggered using Tekton CLI ([tkn]).

## Deploy the pipeline

The pipelines are deployed using [Ansible] and all the necessary files are in
this (`deployment`) directory.

It can be deployed by setting the appropiated credentials at the file
[vars/prod.yml](vars/prod.yml) or [vars/stage.yml](vars/stage.yml),
depending on the environment you like to deploy to.
Then, the deployment is done with `ansible`:

```shell
ansible-playbook main.yml -e env="$ENVIRONMENT"
```

Where `$ENVIRONMENT` is either `prod` or `stage`.

To simplify the workflow and make it more portable, there is a `Makefile` for
running it in a container with all the necessary dependencies. You just need
installed `podman` or `docker`, and `make`.

### Spin stage environment

Run the following command from inside this directory:

```shell
make stage/up
```

### Spin prod environment

Run the following command from inside this directory:

```shell
make prod/up
```

## Add new tasks to the pipeline

The pipeline tasks are defined at the file: [tasks/tasks.yml](tasks/tasks.yml).
Any change to the current tasks or the addition to new task should be made
there.

To understand better how the **tasks** work, it is recomended to look at the
upstream documentation for the [Tekton Tasks].

Then the task should be added to the pipeline at this file:
[tasks/pipeline.yml](tasks/pipeline.yml). Here is the upstream documentation
for the [Tekton Pipelines].

One thing to notice is that the format might be a little different from the
examples on that documentation. That's because this project uses Ansible
for generating all those resources.

[Tekton]: https://tekton.dev/
[Gitlab]: https://gitlab.com
[webhook]: https://docs.gitlab.com/ee/user/project/integrations/webhooks.html
[tkn]: https://tekton.dev/docs/cli/
[Ansible]: https://docs.ansible.com/
[Tekton Tasks]: https://github.com/tektoncd/pipeline/blob/main/docs/tasks.md
[Tekton Pipelines]: https://github.com/tektoncd/pipeline/blob/main/docs/pipelines.md
